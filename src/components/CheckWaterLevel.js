//checks if waterlevel is so high that there is a warning and outputs the warning as string.
//if there is no warning, also a string is outputed
function CheckWaterLevel(props) {
	const waterlevel = Number(props);

	if (waterlevel < 700) {
		return 'Keine Meldestufe'
	}
	else if (waterlevel >= 700 && waterlevel < 740) {
		return 'Meldestufe 1'
	}
	else if (waterlevel >= 740 && waterlevel < 770) {
		return 'Meldestufe 2'
	}	
	else if (waterlevel >= 770 && waterlevel < 850) {
		return 'Meldestufe 3'
	}
	else {
		return 'Meldestufe 4'
	}
}

export default CheckWaterLevel