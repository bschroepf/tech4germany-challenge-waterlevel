//checks if waterlevel is falling, rising or constant and outputs the states as strings
function CheckTrend(props) {
	const trend = Number(props);
	if (trend < 0) {
		return 'fallend';
	}
	else if (trend > 0) {
		return 'steigend'
	}
	else {
		return 'gleichbleibend';
	}
}
export default CheckTrend