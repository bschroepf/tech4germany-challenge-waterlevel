import React, { Component } from 'react';
import CheckWaterLevel from './components/CheckWaterLevel';
import CheckTrend from './components/CheckTrend';


class App extends Component {

    //constructor function
    constructor(props) {
        super(props);
        this.state = {
            items: [], //array of data to be fetched from api
            isLoaded: false,
        }
    }

    //function that fetches api
    componentDidMount() {
        let url = 'https://www.pegelonline.wsv.de/webservices/rest-api/v2/stations.json?fuzzyId=passau&includeTimeseries=true&includeCurrentMeasurement=true'
        fetch(url)
            .then(res => res.json())
            .then(json => {
                this.setState({
                    isLoaded: true,
                    items: json,
                })
            });
    }

    render() {
        var { isLoaded, items } = this.state;

        if (!isLoaded) {
            return <div>Loading...</div>;
        }

        else {
            return (
                <div className="App">
                    <ul>
                        {items.map(item => ( //loop through items
                            <li key={item.uuid}>
                                Name: {item.shortname} | 
                                Waserstand: {item.timeseries[0].currentMeasurement.value} 
                                 ({CheckTrend(item.timeseries[0].currentMeasurement.trend)}) | 
                                {CheckWaterLevel(item.timeseries[0].currentMeasurement.value)}
                            </li>
                            ))}
                    </ul>
                </div>

                );
        }
       
    }

}

export default App;
