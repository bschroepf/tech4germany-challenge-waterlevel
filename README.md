# Waterlevel Passau

Displays relevant information about waterlevels measued at four different stations. More information can be found here: https://github.com/tech4germany/coding_challenge_2020.

## How to run
open terminal, cd to the local repository, then enter
```bash
npm install
npm start
```
The output is displayed at localhost:3000.
Enjoy :)